import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
* У пользователя запросить 2 числа
* size - размер
* type - тип треугольника
* В зависимости от размера и от типа, вывести треугольник соответсвующего вида
* Enter the size: 8
*
*   A					B						C				D
#                    # # # # # # # #      # # # # # # # #                    #
# #                  # # # # # # #          # # # # # # #                  # #
# # #                # # # # # #              # # # # # #                # # #
# # # #              # # # # #                  # # # # #              # # # #
# # # # #            # # # #                      # # # #            # # # # #
# # # # # #          # # #                          # # #          # # # # # #
# # # # # # #        # #                              # #        # # # # # # #
# # # # # # # #      #                                  #      # # # # # # # #
* */

public
class Main {
    public static
    void main ( String[] args ) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter type of triangle and size one:");
        System.out.println("Enter type:");
        String type = bufferedReader.readLine().toUpperCase();
        System.out.println("Enter size:");
        int size = Integer.parseInt(bufferedReader.readLine());
        bufferedReader.close();

        Triangles triangle = new Triangles(type, size);
        triangle.choice();
    }
}

class Triangles {
    String type;
    Integer size;

    public
    Triangles ( String type, Integer size ) {
        if(size < 0) {
            size *= -1;
        }
        this.type = type;
        this.size = size;
    }

    void choice(){
        if(type.equals("A")){
            this.treangleA();
        }
        else if(type.equals("B")){
            this.treangleB();
        }
        else if(type.equals("C")) {
            this.treangleC();
        }
        else if (type.equals("D")){
            this.treangleD();
        }
        else if (type.equals("BC")){
            this.treangleBC();
        }
        else if (type.equals("SUBRB")){
            this.treangleSubrB();
        }
        else{
            System.out.println("You entered not supported type of triangle!\n" +
                    "Therefore you look all types of triangles!");
            this.treangleA();
            this.treangleB();
            this.treangleC();
            this.treangleD();
            this.treangleBC();
            this.treangleSubrB();
        }
    }

    void printHeader(){
        System.out.println("\nTriangle type" + this.type + ". Size: " + this.size +"\n");
    }
    void treangleA(){
        printHeader();
        for(int i = 1; i <= size; i++){
            for(int j = 0; j < i; j++){
                System.out.printf("#");
            }
            System.out.printf("\n");
        }
    }
    void treangleSubrB(){
        printHeader();
        for (int i = size; i >= 1; i--){
            for (int j = size - i; j >= 1; j--){
                System.out.printf(" ");
            }
            for (int k = i; k >= 1; k--){
                System.out.printf("# ");
            }
            System.out.println();
        }
    }

    void treangleBC(){
        printHeader();
        for (int i = size; i >= 1; i--){
            for (int k = i; k >= 1; k--){
                System.out.printf("#");
            }
            for (int j = (size - i) * 2 + 1; j >= 1; j--){
                if (i == size){
                    System.out.printf(" ");
                    break;
                }
                System.out.printf(" ");
            }
            for (int k = i; k >= 1; k--){
                System.out.printf("#");
            }
            System.out.printf("\n");

        }
    }

    void treangleC(){
        printHeader();
        for (int i = size; i >= 1; i--) {
            for (int k = i; k >= 1; k--) {
                System.out.printf("#");
            }
            System.out.printf("\n");
        }
    }

    void treangleB(){
        printHeader();
        for (int i = size; i >= 1; i--){
            for (int j = (size - i) + 1; j > 1; j--){
                if (i == size){
                    System.out.printf(" ");
                    break;
                }
                System.out.printf(" ");
            }
            for (int k = i; k >= 1; k--){
                System.out.printf("#");
            }
            System.out.printf("\n");

        }
    }


    void treangleD(){
        printHeader();
        for (int i = size; i > 0; i--){
            for (int k = i; k > 1; k--){
                System.out.printf(" ");
            }
            for (int j = size - i; j >= 0; j--){
                System.out.printf("#");
            }
            System.out.println();
        }
    }


}
