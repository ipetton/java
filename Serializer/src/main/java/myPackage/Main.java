package myPackage;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.*;

public
class Main {
    public static void main(String[] args) throws Exception {
        List<Map> zooArr = new ArrayList<>();
        ReflectionClass reflectIt = new ReflectionClass();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        String s = "123";
        Zoo zoo = new Zoo(
                (s + ". Обезьяна"),
                (s + ". Попугай"),
                (s + ". Мурка"),
                (s + ". Жучка"),
                (s + ". Волчара"),
                (s + ". Норушка"),
                (s + ". Анатолич"),
                (s + ". Матфевна"),
                (s + ". Внучара Олег")
        );
        System.out.println(reflectIt.customMap(zoo));

        for (int i = 0; i < 20; i++) {
            zooArr.add(reflectIt.customMap(new Zoo(
                    (i + ". Обезьяна"),
                    (i + ". Попугай"),
                    (i + ". Мурка"),
                    (i + ". Жучка"),
                    (i + ". Волчара"),
                    (i + ". Норушка"),
                    (i + ". Анатолич"),
                    (i + ". Матфевна"),
                    (i + ". Внучара Олег")
            )));
        }

        String string = gson.toJson(zooArr);
        System.out.println(string);

    }
}
