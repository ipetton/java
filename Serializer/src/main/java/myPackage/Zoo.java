package myPackage;

class Zoo {
    String monkey;
    String parrot;
    @AddField
    String cat;
    @AddField
    String dog;
    String wolf;
    String mouse;
    @AddField(field = "дедушка")
    String dedka;
    @AddField(field = "Бабуся")
    String babka;
    String vnuchara;

    public Zoo(String monkey, String parrot, String cat,
               String dog, String wolf, String mouse,
               String dedka, String babka, String vnuchara) {
        this.monkey = monkey;
        this.parrot = parrot;
        this.cat = cat;
        this.dog = dog;
        this.wolf = wolf;
        this.mouse = mouse;
        this.dedka = dedka;
        this.babka = babka;
        this.vnuchara = vnuchara;
    }
}
