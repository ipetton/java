package myPackage;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.TreeMap;

public class ReflectionClass {
    public Map customMap(Object object) throws IllegalAccessException {
        return getFielMap(object);
    }


    private Map getFielMap(Object object) throws IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = object.getClass();
        Map<String, String> fieldsMap = new TreeMap<>();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(AddField.class)) {
                fieldsMap.put(getKey(field), (String) field.get(object));
            }
        }

        return fieldsMap;
    }

    private String getKey(Field field) {
        String value = field.getAnnotation(AddField.class).field();
        return value.isEmpty() ? field.getName() : value;
    }
}
