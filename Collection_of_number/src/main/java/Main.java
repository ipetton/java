import java.util.*;

/*
* Базовые операции над коллекциями
- Создать List и наполнить его произвольным количеством случайно сформированных целыц чисел
- Вывести на экран все значения списка если оно кратно 3
- Удалить все элементы, значение которых кратно 3
- Заново вызвать метод вывода на экран значений кратным 3, убедиться что все такие элементы были удалены на прошлом шаге
- Удалить произвольный элемент из списка
- Вставить в начало, середину и конец списка 5 произвольных значений
- Отсортировать значения списка по убыванию
- Вывести на экран только уникальные значения списка
* */

public
class Main {
    public static
    void main ( String[] args ) {
        MyNumbers numbers = new MyNumbers(500);
        numbers.createNumbers();
        numbers.showList();
        numbers.threeDel();
        numbers.showList();
        numbers.three();
        numbers.delRandomElement();
        numbers.showList();
        numbers.addFiveElements();
        numbers.showList();
        numbers.sortDescending();
        numbers.showList();
        numbers.chooseUnique();
        numbers.showList();
    }
}

class MyNumbers {
    List<Integer> numb;
    int max = 0;

    public
    MyNumbers ( int max ) {
        this.max = max;
    }

    // default constructor (if you would't set max of random)
    public
    MyNumbers () {
        this.max = 100;
    }

    void showList(){
        int ellementNumber = 0;
        System.out.println("Elements in List numb: ");
        for (Integer i:
             numb) {
            System.out.printf(ellementNumber + ". ");
            System.out.println(i);
            ellementNumber++;
        }
        System.out.println("___________________________________________");
    }

    // create ArrayList with random Length and random elements
    void createNumbers(){
        Random rnd = new Random(System.currentTimeMillis());
        Integer quantity = rnd.nextInt(max - 1);
        if (quantity < 0) {
            quantity *= -1;
        }
        System.out.println(quantity);
        this.numb = new ArrayList<Integer>();
        for (Integer i = 0; i < quantity; i++){
            numb.add(rnd.nextInt());
        }
    }

    // Method chooses element which are divided by 3 without remainder
    List<Integer> three(){
        ArrayList<Integer> threeArr = new ArrayList<Integer>();
        for (Integer i:
             numb) {
            if (i % 3 == 0)
                threeArr.add(i);
        }
        System.out.println("Element which are divided by 3 without remainder in List numb: ");
        for (Integer i:
             threeArr) {
            System.out.printf(threeArr.indexOf(i) + ". ");
            System.out.println(i);
        }
        System.out.println("___________________________________________");
        return threeArr;
    }

    // Method removes all elements from List numb which are divided by 3 without remainder
    List<Integer> threeDel(){
        ArrayList<Integer>threeArr = (ArrayList<Integer>) three();
        LinkedList<Integer>newNumb = new LinkedList<Integer>();
        newNumb.addAll(numb);
        for (Integer i:
             threeArr) {
            newNumb.remove(i);
        }
        this.numb = newNumb;
        return numb;
    }

    // Method remove random element
    List<Integer> delRandomElement(){
        ArrayList<Integer>serviceArr = new ArrayList<Integer>();
        serviceArr.addAll(numb);
        Random rnd = new Random(System.currentTimeMillis());
        Integer element = serviceArr.get(rnd.nextInt(serviceArr.size()));

        if (numb instanceof ArrayList){
            numb.remove(numb.indexOf(element));
        } else {
            numb.remove(element);
        }
        System.out.println("Element "
        + element + " was removed from List numb");
        System.out.println("___________________________________________");
        return numb;
    }

     /*
      Method adds 5 elements in the beginning of the List,
      5 elements in the end of the List and
      5 elements in the middle of the list
    */
     List<Integer>addFiveElements(){
         LinkedList<Integer>newNumb = (LinkedList) numb;
         Random rnd = new Random(System.currentTimeMillis());
         for (int i = 0; i < 5; i++){
             Integer numStart = rnd.nextInt();
             Integer numMiddle = rnd.nextInt();
             Integer numEnd = rnd.nextInt();
             newNumb.addFirst(numStart);
             newNumb.addLast(numEnd);
             newNumb.add(newNumb.size() >> 1, numMiddle);
         }
         numb = newNumb;
         return numb;
     }

     // Sort descending
     List<Integer> sortDescending(){
         for (int i = 0; i < numb.size(); i++){
             Integer max = numb.get(i);
             for (int j = i; j < numb.size(); j++){
                 if(numb.get(j) > max) {
                     max = numb.get(j);
                     Integer tmp = numb.get(i);
                     numb.set(i, max);
                     numb.set(j, tmp);
                 }
             }
         }

         return numb;
     }

     // Unique elements in List numb
    List<Integer> chooseUnique(){
        Set<Integer> serviceSet = new HashSet<Integer>(numb);
        numb.clear();
        numb.addAll(serviceSet);
        return numb;
    }

}

