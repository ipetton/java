import java.util.ArrayList;

/*
* Создать базовый абстрактный класс - Animal. У животного должна быть кличка и метод voice()
Создать несколько дочерних классов животных, например бегемот, пингвин, кенгуру, зебра, кукушка
Переопределить в дочерних классах метод voice()
Создать интерфейс flyable с методом fly()
Имплементировать этот интерфейс для тех животных для которых он применим

Сгенерировать коллекцию из N объектов животных произвольным образом. Заполнив для каждого кличку уникальным образом.
Т.е в коллекции должны быть объекты всех классов.
Затем, для всех созданных объектов вызвать метод voice(), и если животное умеет летать, то еще и метод fly()
* */

public
class Main {
    public static
    void main ( String[] args ) {
        AnimalCollection collection = new AnimalCollection();
        collection.getZoo();
    }
}

abstract
class Animals {
    String name;

    public
    Animals ( String name ) {
        this.name = name;
    }

    void voice () {
    }
}

interface flyable {
    void fly ();
}

class Hippopotam extends Animals {
    public
    Hippopotam ( String name ) {
        super(name);
    }

    @Override
    void voice () {
        System.out.println("Agrrrr!");
    }
}

class Penguin extends Animals implements flyable {
    public
    Penguin ( String name ) {
        super(name);
    }

    @Override
    void voice () {
        System.out.println("Чырык! Чырык!");
    }

    public
    void fly () {
        System.out.println("I'm prideful bird and I am not flying without kick!");
    }
}

class Kangaroo extends Animals {
    public
    Kangaroo ( String name ) {
        super(name);
    }

    @Override
    void voice () {
        System.out.println("Jump! Jump! Yahoo!");
    }
}

class Zebra extends Animals {
    public
    Zebra ( String name ) {
        super(name);
    }

    @Override
    void voice () {
        System.out.println("Phtrrr! Egogo!");
    }
}

class Cuckoo extends Animals implements flyable {
    public
    Cuckoo ( String name ) {
        super(name);
    }

    public
    void fly () {
        System.out.println("I can fly and I fly!");
    }

    @Override
    void voice () {
        System.out.println("Cuckoo! Cuckoo again and again!");
    }
}

class AnimalCollection {
    private
    ArrayList<Animals> Zoo;

    private
    void
    makeZoo () {
        Zoo = new ArrayList<Animals>();
        Zoo.add(new Hippopotam("Vasya"));
        Zoo.add(new Penguin("Vovan"));
        Zoo.add(new Kangaroo("Kanga"));
        Zoo.add(new Zebra("Mike"));
        Zoo.add(new Cuckoo("Cococh"));
    }

    public
    void getZoo () {
        makeZoo();
        for (Animals animal :
                Zoo) {
            System.out.printf((Zoo.indexOf(animal) + 1) + ". "
                    + animal.getClass() + ": "
                    + "My name is " + animal.name + ". ");
            System.out.printf("I say: ");
            animal.voice();
            if (animal instanceof flyable) {
                System.out.println("I have interface 'flyable':");
                ((flyable) animal).fly();
            }
        }
    }


}